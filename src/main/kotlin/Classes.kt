data class Number(val number: Int, val marked: Boolean = false)

data class Board(val board: Array<Array<Number>>? = null) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Board

        if (!board.contentDeepEquals(other.board)) return false

        return true
    }

    override fun hashCode(): Int {
        return board.contentDeepHashCode()
    }

    fun getScore(): Int {
        return board?.let {
            return@let it.flatten()
                .filter { !it.marked }
                .sumOf { it.number }
        } ?: 0
    }

    fun play(number: Int) {
        mark(number)
    }

    private fun mark(number: Int) {
        board!!.forEachIndexed { row, line ->
            line.forEachIndexed { col, field ->
                if (field.number == number) {
                    board[row][col] = field.copy(marked = true)
                }
            }
        }
    }

    fun isWinner(): Boolean {
        var winner = true
        for (x in 0 until 5) {
            winner = checkRow(x) || checkCol(x)
            if (winner) {
                break
            }
        }
        return winner
    }

    private fun checkRow(rowIndex: Int): Boolean {
        var rowWinner = true
        for (col in 0 until 5) {
            if (!board!![rowIndex][col].marked) {
                rowWinner = false
                break
            }
        }
        return rowWinner
    }

    private fun checkCol(colIndex: Int): Boolean {
        var rowWinner = true
        for (row in 0 until 5) {
            if (!board!![row][colIndex].marked) {
                rowWinner = false
                break
            }
        }
        return rowWinner
    }
}