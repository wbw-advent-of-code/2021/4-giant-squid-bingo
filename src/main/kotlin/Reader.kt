import java.io.File

class Reader {
    fun read(file: String): Pair<List<Int>, List<Board>>{
        val values = File(file).readLines()

        val numbers = values[0].split(",").map { it.toInt() }
        val boards = mutableListOf<Board>()

        for (i in 2 until values.size step 6) {
            boards.add(readBoard(values, i))
        }

        return numbers to boards
    }

    private fun readBoard(flatData: List<String>, line: Int): Board {
        return Board(
            flatData.subList(line, line + 5)
                .mapIndexed { _, row ->
                    row.split(" ")
                        .filter { it.isNotEmpty() }
                        .map { it.toInt() }
                        .map { Number(it) }
                        .toTypedArray()
                }.toTypedArray()
        )
    }
}