val reader = Reader()

fun giantSquidPart1(file: String): Int {
    val pair = reader.read(file)

    val numbers = pair.first
    val boards = pair.second

    val winner = play(numbers, boards)

    return winner.first * winner.second.getScore()
}

fun giantSquidPart2(file: String): Int {
    val pair = reader.read(file)

    val numbers = pair.first
    val boards = pair.second

    val loser = playUntilLastWinner(numbers, boards, null)

    return loser.first * loser.second.getScore()
}

fun playUntilLastWinner(numbers: List<Int>, boards: List<Board>, lastWinner: Pair<Int, Board>?): Pair<Int, Board> {
    for (number in numbers) {
        val optBoard = playBoards(number, boards)

        if (boards.size == 1 && optBoard?.let { return@let true } == true) {
            return number to optBoard
        } else if (optBoard?.let { return@let true } == true) {
            val newNumbers = numbers.slice(numbers.indexOf(number)  until numbers.size)
            val filteredBoards = boards.filter { it != optBoard }
            return playUntilLastWinner(newNumbers, filteredBoards, number to optBoard)
        }
    }
    return lastWinner!!
}

fun play(numbers: List<Int>, boards: List<Board>): Pair<Int, Board> {
    for (number in numbers) {
        val optBoard = playBoards(number, boards)

        if (optBoard?.let { return@let true } == true) {
            return number to optBoard
        }
    }

    throw IllegalStateException("No winning board")
}

fun playBoards(number: Int, boards: List<Board>): Board? {
    for (board in boards) {
        board.play(number)
    }

    for (board in boards) {
        if (board.isWinner()) return board
    }
    return null
}


fun main() {
    println(giantSquidPart1("src/main/resources/input.txt"))
    println(giantSquidPart2("src/main/resources/input.txt"))
}