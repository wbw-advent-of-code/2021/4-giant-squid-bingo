import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class SolutionKtTest {
    val input = "src/test/resources/input.txt"

    @Test
    fun testPart1() {
        assertEquals(4512, giantSquidPart1(input))
    }

    @Test
    fun testPart2() {
        assertEquals(1924, giantSquidPart2(input))
    }
}